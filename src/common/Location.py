import json


class Location:
    def __init__(self, latitude=0, longitude=0, height=0):
        '''
        Location class representing point with GPS coordinates
        @param latitude: latitude
        @param longitude: longitude
        @param height: height above the surface of map
        '''
        self.latitude = latitude
        self.longitude = longitude
        self.height = height
        self.gridCoordinates = []

    def getLatitude(self):
        return self.latitude;

    def getLongitude(self):
        return self.longitude;

    def getHeight(self):
        return self.height;

    def setLatitude(self, lat):
        self.latitude = lat
        return;

    def setLongitude(self, lon):
        self.longitude = lon
        return;

    def setHeight(self, hei):
        self.height = hei
        return;

    def toJson(self):
        data = {}
        data['latitude'] = self.getLatitude()
        data['longitude'] = self.getLongitude()
        data['height'] = self.getHeight()
        json_data = json.dumps(data)
        return json_data
