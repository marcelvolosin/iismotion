from src.common.Location import Location
from src.placeable.Placeable import Placeable
from src.common.GridCell import GridCell


class SpaceGrid:
    # X = Latitude
    # Y = Longitude

    def __init__(self, rows, latmin, latmax, lonmin, lonmax):
        self.rows = rows
        self.latmin = latmin
        self.latmax = latmax
        self.lonmin = lonmin
        self.lonmax = lonmax
        self.latStep = abs(latmax - latmin) / rows
        self.lonStep = abs(lonmax - lonmin) / rows

        self.grid = [[0 for x in range(rows)] for y in range(rows)]
        for x in range(0, rows):
            for y in range(0, rows):
                self.grid[x][y] = GridCell()

    def getGridCoordinates(self, location=Location):
        x = int((location.latitude - self.latmin) // self.latStep)
        y = int((location.longitude - self.lonmin) // self.lonStep)
        return x, y

    def getGridCoordinatesAsList(self, location=Location):
        x, y = self.getGridCoordinates(location)
        return [x, y]

    def updatePersonsGridCoordinates(self, persons):
        for person in persons:
            person.location.gridCoordinates = self.getGridCoordinatesAsList(person.location)

    def addToGrid(self, item=Placeable):
        location = item.location
        x = int((location.latitude - self.latmin) // self.latStep)
        y = int((location.longitude - self.lonmin) // self.lonStep)

        if (item.__class__.__name__ == 'MacroCell'):
            self.grid[x][y].macrocells.add(item)
        else:
            self.grid[x][y].femtocells.add(item)
        item.location.gridCoordinates = [x, y]

    def addAllToGrid(self, items):
        for item in items:
            self.addToGrid(item)


    def removeFromGrid(self, item=Placeable):
        x = item.location.gridCoordinates[0]
        y = item.location.gridCoordinates[1]

        if (item.__class__.__name__ == 'MacroCell'):
            self.grid[x][y].macrocells.remove(item)
        else:
            self.grid[x][y].femtocells.remove(item)
        item.location.gridCoordinates = []


    def updateGridCoordinates(self, item=Placeable):
        if (self.getGridCoordinates(item) != item.location.gridCoordinates):
            self.removeFromGrid(item)
            self.addToGrid(item)

    def getNearestMacrocells(self, dist, location=Location):

        x, y = self.getGridCoordinates(location)
        result = []
        for xx in range(x - dist, x + dist + 1):
            for yy in range(y - dist, y + dist + 1):
                if (xx >= 0 and yy >= 0 and xx < self.rows and yy < self.rows):
                    result = result + list(self.grid[xx][yy].macrocells)

        if (len(result) == 0 and dist < self.rows):
            result = self.getNearestMacrocells(dist + 1, location)
        return result;

    def getNearestFemtocells(self, dist, location=Location):
        x, y = self.getGridCoordinates(location)
        result = []
        for xx in range(x - dist, x + dist + 1):
            for yy in range(y - dist, y + dist + 1):
                if (xx >= 0 and yy >= 0 and xx < self.rows and yy < self.rows):
                    result = result + list(self.grid[xx][yy].femtocells)

        if (len(result) == 0 and dist < self.rows):
            result = self.getNearestFemtocells(dist + 1, location)
        return result;