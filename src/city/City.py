import osmnx as ox
import networkx as nx
import pandas as pd
from src.common.CommonFunctions import CommonFunctions
from src.common.Location import Location
from src.city.Building import Building


class City:
    def __init__(self, radius, oneWayEnabled=False, location=Location):
        '''

        @param radius: import map in radius from location
        @param location: center location of map that will be imported
        '''

        ox.config(use_cache=True, log_console=True)
        ox.__version__

        self.comm = CommonFunctions()
        self.driveGraph = ox.graph_from_point((location.latitude, location.longitude), distance=radius,
                                              network_type='drive', simplify=False)
        if (oneWayEnabled == False):
            self.driveGraph = self.driveGraph.to_undirected()


        self.gdfNodes, self.gdfEdges = ox.graph_to_gdfs(self.driveGraph)
        self.gdfBuildings = ox.buildings_from_point((location.latitude, location.longitude), distance=radius)

        # replace NaN in buildings heights with the average height
        self.gdfBuildings['height'] = pd.to_numeric(self.gdfBuildings['height'], errors='force')
        self.gdfBuildings['height'].fillna((self.gdfBuildings['height'].mean()), inplace=True)

        self.buildings = self.parseBuildings(self.gdfBuildings)

        self.latitudeInterval = [self.gdfNodes['y'].min(), self.gdfNodes['y'].max()]
        self.longitudeInterval = [self.gdfNodes['x'].min(), self.gdfNodes['x'].max()]

    def getLocationOfRandomNode(self):
        node = self.gdfNodes.sample(n=1)
        ran_loc = Location()
        ran_loc.setLatitude(node.iloc[0]['y'])
        ran_loc.setLongitude(node.iloc[0]['x'])
        return ran_loc;

    def getRoute(self, locationA=Location, locationB=Location):
        orig_node = ox.get_nearest_node(self.driveGraph, (locationA.getLatitude(), locationA.getLongitude()))
        dest_node = ox.get_nearest_node(self.driveGraph, (locationB.getLatitude(), locationB.getLongitude()))
        route = nx.shortest_path(self.driveGraph, orig_node, dest_node, weight='length')
        return route;

    def routeToLocations(self, route):
        locList = []
        nodes = self.gdfNodes.loc[route]
        for i in range(0, len(nodes.index)):
            loc = Location()
            loc.setLatitude(nodes.iloc[i]['y'])
            loc.setLongitude(nodes.iloc[i]['x'])
            locList.append(loc)
        return locList

    def getClosestNodeLocation(self, loc=Location):
        node = ox.get_nearest_node(self.driveGraph, (loc.getLatitude(), loc.getLongitude()))
        loc = Location()
        loc.setLatitude(node.iloc[0]['y'])
        loc.setLongitude(node.iloc[0]['x'])
        return loc

    def isInsideBuilding(self, point=Location):
        for building in self.buildings:
            if (building.pointInBuilding(point)):
                return True;
        return False;

    def getOnBuildingHeight(self, point=Location):
        for building in self.buildings:
            if (building.pointInBuilding(point)):
                return building.height, building.id
        return 0, 0;

    def parseBuildings(self, gdfBuild):
        buildings = []
        for index, row in gdfBuild.iterrows():
            building = Building()
            building.setHeight(row["height"])
            geom = []
            lon, lat = row["geometry"].exterior.coords.xy
            for i in range(0, len(lon)):
                location = Location()
                location.setLatitude(float(lat[i]))
                location.setLongitude(float(lon[i]))
                geom.append(location)
            building.geometryLocations = geom
            building.getCentroid()
            buildings.append(building)
        return buildings


    # Plotting
    def plotRoute(self, route, name):
        fig, ax = ox.plot_graph_route(self.driveGraph, route, node_size=0.4, edge_linewidth=0.3, orig_dest_node_size=2,
                                      save=True, show=False, route_linewidth=0.5, file_format='pdf',
                                      filename=self.comm.getTimestamp() + name)

    def plotCity(self, type, name):
        if type == 'drive':
            fig, ax = ox.plot_graph(self.driveGraph, fig_height=30, node_size=0.4, edge_linewidth=0.3, save=True,
                                    show=False,
                                    file_format='pdf', filename=self.comm.getTimestamp() + name)
        if type == 'buildings':
            fig, ax = ox.plot_buildings(ox.project_gdf(self.gdfBuildings), save=True, show=False, file_format='pdf',
                                        filename=self.comm.getTimestamp() + name)