from src.placeable.Placeable import Placeable
from src.common.Location import Location


class Movable(Placeable):
    def __init__(self):
        super(Movable, self).__init__()
        self.speed = 0
        self.originLocation = Location()
        self.destinationLocation = Location()
        self.movementsMade = 0

    def getSpeed(self):
        return self.speed;

    def setSpeed(self, sp):
        self.speed = sp;