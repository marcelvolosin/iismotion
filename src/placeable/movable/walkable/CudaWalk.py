from __future__ import division
from numba import cuda
import numpy
import math

from src.placeable.movable.walkable import Person
from src.placeable.movable.walkable.Vehicles.Bus import Bus
from src.placeable.movable.walkable.Vehicles.Vehicle import Vehicle


# CUDA kernel
@cuda.jit
def my_kernel(io_array):
    def toRadians(degrees):
        return degrees * math.pi / 180

    def toDegrees(radians):
        return radians * 180 / math.pi

    pos = cuda.grid(1)
    if pos < io_array.size:
        lat1 = io_array[pos, 0]
        lon1 = io_array[pos, 1]
        lat2 = io_array[pos, 2]
        lon2 = io_array[pos, 3]
        distanceToWalk = io_array[pos, 4]

        d = distanceToWalk
        R = 6370000

        dLat = toRadians(lat2 - lat1)
        dLon = toRadians(lon2 - lon1)
        lat1rad = toRadians(lat1)
        lat2rad = toRadians(lat2)

        a = math.sin(dLat / 2) * math.sin(dLat / 2) + math.sin(dLon / 2) * math.sin(dLon / 2) * math.cos(
            lat1) * math.cos(lat2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
        distanceBetweenPoints = R * c

        io_array[pos, 8] = distanceBetweenPoints

        if (distanceToWalk > distanceBetweenPoints):
            io_array[pos, 5] = lat2
            io_array[pos, 6] = lon2
            io_array[pos, 7] = True
        else:
            io_array[pos, 7] = False
            lat1rad = toRadians(lat1)
            lon1rad = toRadians(lon1)

            lat2rad = toRadians(lat2)
            diffLong = toRadians(lon2 - lon1)

            x = math.sin(diffLong) * math.cos(lat2rad)
            y = math.cos(lat1rad) * math.sin(lat2rad) - (math.sin(lat1rad) * math.cos(lat2rad) * math.cos(diffLong))
            initial_bearing = math.atan2(x, y)
            initial_bearing = toDegrees(initial_bearing)
            brng = toRadians((initial_bearing + 360) % 360)

            lat3rad = math.asin(
                math.sin(lat1rad) * math.cos(d / R) + math.cos(lat1rad) * math.sin(d / R) * math.cos(brng))
            lon3rad = lon1rad + math.atan2(math.sin(brng) * math.sin(d / R) * math.cos(lat1rad),
                                           math.cos(d / R) - math.sin(lat1rad) * math.sin(lat2rad))
            lat3 = toDegrees(lat3rad)
            lon3 = toDegrees(lon3rad)

            io_array[pos, 5] = lat3
            io_array[pos, 6] = lon3


class CudaWalk():

    def __init__(self, movementModel):
        print("Cuda walk: present GPU-s:", cuda.gpus)
        cuda.select_device(0)
        self.movementModel = movementModel
        self.noOfWalkables = 0
        self.npFrame = numpy.zeros(shape=(self.noOfWalkables, 8))
        self.npInitialized = False

    def updateFrameSize(self):
        self.noOfWalkables = len(self.movementModel.walkables)
        self.npFrame = numpy.zeros(shape=(self.noOfWalkables, 8))

    def walk(self):
        for i in range(self.noOfWalkables):
            if (len(self.movementModel.walkables[i].locationRoute) == 0 and isinstance(self.movementModel.walkables[i],
                                                                                       Bus) == False):
                self.movementModel.walkables[i].updateRoute()

            self.npFrame[i, 0] = self.movementModel.walkables[i].location.latitude
            self.npFrame[i, 1] = self.movementModel.walkables[i].location.longitude
            self.npFrame[i, 2] = self.movementModel.walkables[i].locationRoute[0].latitude
            self.npFrame[i, 3] = self.movementModel.walkables[i].locationRoute[0].longitude
            self.npFrame[i, 4] = self.movementModel.walkables[i].speed
            self.npFrame[i, 5] = 0
            self.npFrame[i, 6] = 0
            self.npFrame[i, 7] = False

        threadsperblock = 256
        blockspergrid = math.ceil(self.npFrame.shape[0] / threadsperblock)
        my_kernel[blockspergrid, threadsperblock](self.npFrame)


        for i in range(self.noOfWalkables):
            # print("=================================")
            # print("OLD latitude:  ", self.movementModel.walkables[i].location.latitude)
            # print("OLD longitude: ", self.movementModel.walkables[i].location.longitude)
            self.movementModel.walkables[i].location.latitude = self.npFrame[i, 5]
            self.movementModel.walkables[i].location.longitude = self.npFrame[i, 6]

            # print("NEW latitude:  ", self.movementModel.walkables[i].location.latitude)
            # print("NEW longitude: ", self.movementModel.walkables[i].location.longitude)
            # print("target reached: ", self.npFrame[i, 7])

            if (self.npFrame[i, 7] == True):
                if (isinstance(self.movementModel.walkables[i], Bus)):
                    self.movementModel.walkables[i].locationRoute.append(self.locationRoute[0])

                self.movementModel.walkables[i].locationRoute.pop(0)

            if (isinstance(self.movementModel.walkables[i], Vehicle)):
                self.movementModel.walkables[i].updatePassangersLocation(self)
