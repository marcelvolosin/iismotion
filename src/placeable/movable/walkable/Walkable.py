from src.city.City import City
from src.placeable.movable.Movable import Movable
from src.common.CommonFunctions import CommonFunctions
from copy import copy, deepcopy

class Walkable(Movable):

    def __init__(self):
        super(Walkable, self).__init__()
        self.inVehicle = False;
        self.route = []
        self.locationRoute = []
        self.com = CommonFunctions()

        return

    def setInVehicle(self, inVeh):
        self.inVehicle = inVeh
        return

    def getInVehicle(self):
        return self.inVehicle

    def walk(self):
        if (self.inVehicle == False):
            # print(">>Walk called by Walkable with ID: ", self.id)
            # print("(so far made ", self.movementsMade, "movements)")
            # print("Current location: " + self.location.toJson(), " (becoming origin location)")
            self.originLocation = deepcopy(self.location)
            distanceToWalk = self.speed
            # print("Going to walk ", distanceToWalk, " meters")

            while (distanceToWalk > 0):
                # print("while| ", distanceToWalk, "m still remaining")
                if len(self.locationRoute) == 0:
                    self.updateRoute()
                    # print("locationRoute was empty, therfore this is new one updated")
                    # print(self.locationRoute)

                self.destinationLocation = self.locationRoute[0]
                # print("destination to walk is: " + self.destinationLocation.toJson())
                distanceToDestination = self.com.getReal2dDistance(self.location, self.destinationLocation)
                # print("distance to given location is: ", distanceToDestination)
                if (distanceToDestination < distanceToWalk):
                    # print("it was closer than planned walk this iteration, therfore")
                    distanceToWalk = distanceToWalk - distanceToDestination
                    self.location = self.destinationLocation
                    # print("moving to new location ", self.location)
                    # print("with remaining distance to walk this iteration: ", distanceToWalk)
                    self.locationRoute.pop(0)
                else:
                    self.location = self.com.getLocationFromPathAndDist(distanceToWalk, self.location,
                                                                        self.destinationLocation)
                    # print("Managed to move desired distance, my new location is: ", self.location.toJson())
                    distanceToWalk = 0

            self.movementsMade = self.movementsMade + 1
        return;

    def updateRoute(self):
        self.route = self.city.getRoute(self.location, self.city.getLocationOfRandomNode())
        self.locationRoute = self.city.routeToLocations(self.route)
        return;