from src.common.Location import Location
from src.city.City import City
from src.placeable.movable.walkable.Person import Person
from src.placeable.movable.walkable.Vehicles.Car import Car
from src.placeable.movable.walkable.Vehicles.Bus import Bus
from src.common.CommonFunctions import CommonFunctions
from src.common.MacrocellLoader import MacrocellLoader
from src.common.FrontendServer import FrontendServer
from src.common.SpaceGrid import SpaceGrid
from src.placeable.movable.walkable.CudaWalk import CudaWalk
from random import randint
from src.placeable.stationary.FemtoCell import FemtoCell

import json


class MovementModel:

    def __init__(self, guiEnabled, radius, oneWayEnabled=False, location=Location, nets=None):
        '''
        Highest level class of simulated model
        @param guiEnabled: True/False enable GUI updates to JS frontend
        @param radius: import map in radius from location
        @param location: center location of map that will be imported
        '''
        self.nets = nets
        self.guiEnabled = guiEnabled
        self.cudaEnabled = False
        self.city = City(radius, oneWayEnabled, location)
        self.com = CommonFunctions()
        self.macrocellLoader = MacrocellLoader()
        self.walkables = []

        self.BTSs = []
        self.femtocells = []
        self.macrocells = self.macrocellLoader.getMacrocells("147.232.40.82",
                                                             self.city.latitudeInterval[0],
                                                             self.city.latitudeInterval[1],
                                                             self.city.longitudeInterval[0],
                                                             self.city.longitudeInterval[1],
                                                             nets)
        self.BTSs = self.BTSs + self.macrocells
        self.updateCellHeights(self.BTSs)
        self.grid = SpaceGrid(5, self.city.latitudeInterval[0], self.city.latitudeInterval[1],
                              self.city.longitudeInterval[0], self.city.longitudeInterval[1])
        self.grid.addAllToGrid(self.macrocells)

        if (self.guiEnabled):
            self.frontend = FrontendServer()
            self.sendUpdateToFrontend(True)
        self.cudaWalker = CudaWalk(self)


    def setCudaEnabled(self, value):
        '''
        Enable/Disable usage of CUDA cores for computation of GPS coordinates for moving objects
        @param value: True/False
        @return: no return value
        '''
        self.cudaEnabled = value

    def updateCellHeights(self, cells):
        '''
        updates heights of given Base stations according to buildings
        :param cells: list of BTSs to be updated
        :return: no
        '''
        sumHeights = 0.0
        count = 0
        for cell in cells:
            cell.location.height, buildingId = self.city.getOnBuildingHeight(cell.location)
            if (cell.location.height != 0):
                cell.onBuilding = buildingId
                sumHeights = sumHeights + cell.location.height
                count = count + 1

        avgHeight = sumHeights / count
        for cell in self.macrocells:
            if (cell.location.height == 0):
                cell.location.height = avgHeight

    def addFemtoCellsToModel(self, count, minRadius, height, fixHeight=False):
        '''
        Function that creates femtocells
        :param count: number of desired femtocells
        :param minRadius: minimum distance from other femtocells
        :param height: height of femtocell deployment
        :return: returns a list of created femtocells
        '''
        locations = []
        for cell in self.femtocells:
            locations.append(cell.location)

        madeFemtocells = []
        for i in range(0, count):
            location = self.com.getRandomLocationWithinCity(self.city.latitudeInterval, self.city.longitudeInterval,
                                                            height)
            while (self.com.getShortestDistanceFromLocations(locations, location) < minRadius):
                location = self.com.getRandomLocationWithinCity(self.city.latitudeInterval, self.city.longitudeInterval,
                                                                height)

            locations.append(location)
            femtocell = FemtoCell()
            femtocell.location = location
            madeFemtocells.append(femtocell)

        if (fixHeight):
            self.updateCellHeights(madeFemtocells)

        for cell in madeFemtocells:
            if cell.location.height == 0:
                cell.location.height = height

        self.BTSs = self.BTSs + madeFemtocells
        self.grid.addAllToGrid(madeFemtocells)
        return madeFemtocells


    def addPersonsToModel(self, count, withInitialMove=False):
        '''
        adds persons to the simulation model with the random location within the simulated space
        @param count: number of persons to be added
        @param withInitialMove: True/False make (0,500) steps in random direction to leave the initial location
        @return: no return value
        '''
        for i in range(0, count):
            location = self.city.getLocationOfRandomNode()
            person = Person()
            person.setSpeed(3)
            person.setCity(self.city)
            person.setLocation(location)
            if withInitialMove:
                for i in range(0, randint(0, 500)):
                    person.walk()
            self.walkables.append(person)
        self.cudaWalker.updateFrameSize()

    def addCarsToModel(self, count, passangers, withInitialMove=False):
        '''
        add cars to the model, with random location
        @param count: number of cars to be added
        @param passangers: number of passangers each car will contaion
        @param withInitialMove: True/False make (0,500) steps in random direction to leave the initial location
        @return: no return value
        '''
        for i in range(0, count):
            location = self.city.getLocationOfRandomNode()
            car = Car()
            car.setCity(self.city)
            car.setLocation(location)
            car.setSpeed(10)
            for j in range(0, passangers):
                person = Person()
                person.setCity(self.city)
                self.walkables.append(person)
                car.addPerson(person)
            if withInitialMove:
                for i in range(0, randint(0, 500)):
                    car.walk()
            self.walkables.append(car)

        self.cudaWalker.updateFrameSize()

    def addBusToModel(self, passangers, withInitialMove=False, bus=Bus):
        '''
        adds configured instance of Bus to the model
        @param passangers: number of passangers to be added
        @param withInitialMove: True/False make (0,500) steps in random direction to leave the initial location
        @param bus: Bus instance itself
        @return: no return value
        '''
        bus.setCity(self.city)
        bus.setMaxCapacity(60)
        bus.setSpeed(15)

        for j in range(0, passangers):
            person = Person()
            person.setCity(self.city)
            self.walkables.append(person)
            bus.addPerson(person)

        self.walkables.append(bus)
        bus.makeRouteFromStops()
        self.cudaWalker.updateFrameSize()

    def walkStep(self):
        '''
        moves all added Movables =  Persons, Cars and Busses
        @return: no return value
        '''
        if (self.cudaEnabled == True):
            self.cudaWalker.walk()
        else:
            for walkable in self.walkables:
                walkable.walk()

        self.grid.updatePersonsGridCoordinates(self.walkables)
        self.sendUpdateToFrontend(False)

    def saveMovableLocationsToFile(self):
        '''
        saves locations of all Movables (Persons, Cars and Busses) to the .csv file named movLocations
        @return: no return
        '''
        for w in self.walkables:
            self.com.appendToFile("movLocations",
                                  str(w.getLocation().getLatitude()) + "," + str(
                                      w.getLocation().getLongitude()) + "," + str(w.movementsMade) + "\n")

    def saveMacrocellLocationsToFile(self):
        '''
        saves locations of macrocells to the .csv file named macrocellLocations
        @return: no return
        '''
        for bts in self.macrocells:
            self.com.appendToFile("macrocellLocations",
                                  str(bts.getLocation().getLatitude()) + "," + str(
                                      bts.getLocation().getLongitude()) + "," + str(bts.location.height) + "\n")

    def saveBuildingLocsToFile(self):
        '''
        saves locations of all buildings' nodes to the .csv file named buildings
        @return: no return
        '''
        for b in self.city.buildings:
            for loc in b.geometry:
                self.com.appendToFile("buildings",
                                      str(loc.latitude) + "," + str(
                                          loc.longitude) + "," + str(b.id) + "\n")


    def sendUpdateToFrontend(self, initialUpdate):
        '''
        sends update of locations to the frontend
        @param initialUpdate: True - sends locations of static objects (buildings/macrocells) | False - send update of movables' locations
        @return: no return
        '''
        if (self.guiEnabled):
            data = {}
            data["type"] = "FeatureCollection"

            features = []

            if (initialUpdate):
                # for b in self.city.buildings:
                #     features.append(b.getGeoJson())
                #
                for bts in self.BTSs:
                    features.append(bts.getGeoJson())

            for w in self.walkables:
                features.append(w.getGeoJson())

            data["features"] = features

            if (len(features) > 0):
                json_data = json.dumps(data)
                self.frontend.addMessageToQueue(json_data)
                # self.com.appendToFile("json", json_data)